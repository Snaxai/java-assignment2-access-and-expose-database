## Access and expose a database
In this assignement we will access and expose a database. The database we will use is Chinook sqlite database. The project utilizes JDBC to read from and write to this database. Additionally, the application uses Thymeleaf to provide a page for searching for songs.

### Resources
ERD
```https://www.sqlitetutorial.net/sqlite-sample-database/```

### Heroku
The assignment is hosted on Heroku as a Docker container

```https://java-access-and-expose-db.herokuapp.com/music```
```https://java-access-and-expose-db.herokuapp.com/api/v1/customers```

### Postman collection

https://gitlab.com/Snaxai/java-assignment2-access-and-expose-database/-/blob/main/Postman%20collection/Access%20and%20expose%20a%20database.postman_collection.json

## Author
Daniel Mossestad

Trym Ellingsen
