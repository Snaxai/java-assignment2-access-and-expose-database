const searchButton = document.getElementById("searchButton");
const searchInput = document.getElementById("searchInput");
const errorDivEl = document.getElementById("errorDiv");
const resultHeader = document.getElementById("resultHeader");

let errorMessage;

const search = () => {
    errorDivEl.innerText = "";
    searchInputValue = searchInput.value.trim()
    if (searchInputValue) {
        location.pathname = "music/result/" + searchInputValue
    } else {
        errorMessage = "input can not be empty"
        errorDivEl.innerText = errorMessage
    }
}