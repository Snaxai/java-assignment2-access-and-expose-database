package com.example.javaassignment2accessandexposedatabase.repositories.music;

import com.example.javaassignment2accessandexposedatabase.models.Artist;
import com.example.javaassignment2accessandexposedatabase.models.Genre;
import com.example.javaassignment2accessandexposedatabase.models.Song;
import com.example.javaassignment2accessandexposedatabase.models.Track;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MusicRepositoryImpl implements MusicRepository {
    private final String connectionString = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";

    /**
     * Get a song based on a search query
     *
     * @param songName Name of the song
     * @return The first song matching the search query
     */
    @Override
    public List<Song> getSongsByName(String songName) {
        List<Song> returnSongs = new ArrayList<>();
        // Open a connection (try with resources)
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            // Prepare statement
            String sql =
                    "select trackid, t.Name as artistName, a2.Name as albumName, a.Title, g.Name as genreName\n" +
                            "from Track t\n" +
                            "inner join Album A on A.AlbumId = t.AlbumId\n" +
                            "inner join Artist A2 on A2.ArtistId = A.ArtistId\n" +
                            "inner join Genre G on G.GenreId = t.GenreId\n" +
                            "where t.Name LIKE ?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, "%" + songName + "%");
            // Handle result
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Song song = new Song(
                        resultSet.getInt("trackId"),
                        resultSet.getString("artistName"),
                        resultSet.getString("albumName"),
                        resultSet.getString("Title"),
                        resultSet.getString("genreName")
                );
                returnSongs.add(song);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (returnSongs.size() == 0)
            // Return this when nothing is found in database
            returnSongs.add(new Song(0, "Could not find a song", "no song", "no song", "no song"));
        return returnSongs;
    }

    /**
     * Get random tracks
     *
     * @param limit Number of tracks to get
     * @return Tracks
     */
    @Override
    public List<Track> getRandomTracks(int limit) {
        List<Track> trackList = new ArrayList<>();
        String sql = "SELECT trackId, name FROM Track " +
                "ORDER BY RANDOM() LIMIT ?";
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            // Prepare statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, limit);
            // Handle result
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Track track = new Track(
                        resultSet.getInt("trackId"),
                        resultSet.getString("name")
                );
                trackList.add(track);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return trackList;
    }

    /**
     * Get random artists
     *
     * @param limit Number of artists to get
     * @return Artists
     */
    @Override
    public List<Artist> getRandomArtists(int limit) {
        List<Artist> artists = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            // Prepare statement
            String sql = "SELECT * FROM Artist " +
                    "ORDER BY RANDOM() LIMIT ?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, limit);
            // Handle result
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Artist artist = new Artist(
                        resultSet.getInt("artistId"),
                        resultSet.getString("name")
                );
                artists.add(artist);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return artists;
    }

    /**
     * Get random genres
     *
     * @param limit number of genres to get
     * @return Genres
     */
    @Override
    public List<Genre> getRandomGenres(int limit) {
        List<Genre> genres = new ArrayList<>();
        String sql = "SELECT genreId, name FROM Genre " +
                "ORDER BY RANDOM() LIMIT ?";
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            // Prepare statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, limit);
            // Handle result
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Genre genre = new Genre(
                        resultSet.getInt("genreId"),
                        resultSet.getString("name")
                );
                genres.add(genre);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return genres;
    }

    @Override
    public Song getSongById(int id) {
        return null;
    }
}
