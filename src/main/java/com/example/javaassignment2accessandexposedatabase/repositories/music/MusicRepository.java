package com.example.javaassignment2accessandexposedatabase.repositories.music;

import com.example.javaassignment2accessandexposedatabase.models.Artist;
import com.example.javaassignment2accessandexposedatabase.models.Genre;
import com.example.javaassignment2accessandexposedatabase.models.Song;
import com.example.javaassignment2accessandexposedatabase.models.Track;

import java.util.List;

public interface MusicRepository {
    List<Song> getSongsByName(String name);

    List<Track> getRandomTracks(int limit);

    List<Artist> getRandomArtists(int limit);

    List<Genre> getRandomGenres(int limit);

    Song getSongById(int id);
}
