package com.example.javaassignment2accessandexposedatabase.repositories.customer;

import com.example.javaassignment2accessandexposedatabase.models.Customer;
import com.example.javaassignment2accessandexposedatabase.models.CustomerCountry;
import com.example.javaassignment2accessandexposedatabase.models.CustomerGenre;
import com.example.javaassignment2accessandexposedatabase.models.CustomerSpender;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {
    private final String connectionString = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";

    /**
     * Get all customers
     *
     * @return A list of all customers
     */
    @Override
    public List<Customer> getAllCustomers() {
        String sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM customer";
        return getCustomersFromDatabase(sql);
    }

    /**
     * Add a customer
     *
     * @param customer Customer info to be added
     * @return The customer added
     */
    @Override
    public Customer addCustomer(Customer customer) {
        int result = 0; // Mabye use this for error handling
        // Open a connection (try with resources)
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            // Prepare statement
            String sql =
                    "INsert into customer(" +
                            "CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email)" +
                            " values (?,?,?,?,?,?,?)";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customer.id());
            statement.setString(2, customer.firstName());
            statement.setString(3, customer.lastName());
            statement.setString(4, customer.country());
            statement.setString(5, customer.postalCode());
            statement.setString(6, customer.phoneNumber());
            statement.setString(7, customer.email());
            // Handle result
            result = statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return customer;
    }

    /**
     * Get a customer by id
     *
     * @param customerId id of a customer
     * @return A customer
     */
    @Override
    public Customer getCustomerById(int customerId) {
        Customer returnCustomer = null; // Mabye use this for error handling
        // Open a connection (try with resources)
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            // Prepare statement
            String sql =
                    "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM customer WHERE customerId = ?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customerId);
            // Handle result
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                returnCustomer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return returnCustomer;
    }

    /**
     * Get customer by name
     *
     * @param customerName Name of customer
     * @return A customer
     */
    @Override
    public Customer getCustomerByName(String customerName) {
        Customer returnCustomer = null; // Mabye use this for error handling
        // Open a connection (try with resources)
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            // Prepare statement
            String sql =
                    "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM customer " +
                            "WHERE firstname LIKE ? OR lastname LIKE ?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, "%" + customerName + "%");
            statement.setString(2, "%" + customerName + "%");
            // Handle result
            ResultSet resultSet = statement.executeQuery();
            returnCustomer = new Customer(
                    resultSet.getInt("CustomerId"),
                    resultSet.getString("FirstName"),
                    resultSet.getString("LastName"),
                    resultSet.getString("Country"),
                    resultSet.getString("PostalCode"),
                    resultSet.getString("Phone"),
                    resultSet.getString("Email")
            );

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return returnCustomer;
    }

    /**
     * Get customers with a limit and an offset
     *
     * @param limit  How many customers to get
     * @param offset What row to start from
     * @return List of customers
     */
    @Override
    public List<Customer> getCustomersWithLimitAndOffset(int limit, int offset) {
        List<Customer> customers = new ArrayList<>();
        // Open a connection (try with resources)
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            // Prepare statement
            String sql =
                    "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer " +
                            "LIMIT ? OFFSET ?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, limit);
            statement.setInt(2, offset);
            // Handle result
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Customer customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
                customers.add(customer);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return customers;
    }

    /**
     * Get number of customers in each country
     *
     * @return a list of countries and numbers of customers in each country
     */
    @Override
    public List<CustomerCountry> getNumberOfCustomersInEachCountry() {
        String sql = "SELECT BillingCountry, count(DISTINCT CustomerId) AS NumberOfCustomers " +
                "FROM Invoice " +
                "GROUP BY BillingCountry " +
                "ORDER BY NumberOfCustomers desc, BillingCountry asc";
        List<CustomerCountry> customerCountry = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            // Prepare statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Handle result
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                customerCountry.add(new CustomerCountry(resultSet.getString("BillingCountry"),
                        resultSet.getInt("NumberOfCustomers")));
            }


        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return customerCountry;
    }

    /**
     * Get a list of customers sorted on highest spenders desc
     *
     * @return a list of customers ordered by highest spenders
     */
    @Override
    public List<CustomerSpender> getHighestSpenders() {
        String sql =
                "SELECT C.CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email, total(I.Total) as Spent " +
                        "FROM Customer C " +
                        "INNER JOIN Invoice I " +
                        "WHERE C.CustomerId = I.CustomerId " +
                        "GROUP BY c.CustomerId " +
                        "ORDER BY Spent DESC";
        List<CustomerSpender> customerSpenders = new ArrayList<>();
        // Open a connection (try with resources)
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            // Prepare statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Handle result
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Customer customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
                customerSpenders.add(new CustomerSpender(customer, resultSet.getDouble("Spent")));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return customerSpenders;
    }

    /**
     * Get the most popular genre for a specific customer
     *
     * @param customerId The id of the customer
     * @return List of most popular genre(s)
     */
    @Override
    public List<CustomerGenre> getMostPopularGenre(int customerId) {
        List<CustomerGenre> mostPopularGenre = new ArrayList<>();
        String sql =
                "SELECT G.Name as GenreName, count(G.GenreId) as TimesBought " +
                        "FROM Customer C " +
                        "    INNER JOIN Invoice I on C.CustomerId = I.CustomerId " +
                        "    INNER JOIN InvoiceLine IL on I.InvoiceId = IL.InvoiceId " +
                        "    INNER JOIN Track T on T.TrackId = IL.TrackId " +
                        "    INNER JOIN Genre G on G.GenreId = T.GenreId " +
                        "WHERE C.CustomerId = ? " +
                        "GROUP BY C.CustomerId, G.GenreId " +
                        "ORDER BY TimesBought desc";
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            // Prepare statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customerId);
            // Handle result
            ResultSet resultSet = statement.executeQuery();
            int timesBought = resultSet.getInt("TimesBought");
            int maxTimesBought = timesBought;
            while (resultSet.next()) {
                CustomerGenre genre = new CustomerGenre(resultSet.getString("GenreName"), timesBought);
                timesBought = resultSet.getInt("TimesBought");
                if (timesBought != maxTimesBought)
                    break;
                mostPopularGenre.add(genre);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return mostPopularGenre;
    }


    /**
     * Updates a customer
     *
     * @param customerId Id of the customer we want to update
     * @param customer Information to add to updated customer
     * @return Returns updated customer on success, null on failure.
     */
    @Override
    public Customer updateCustomer(int customerId, Customer customer) {
        String sql = "UPDATE Customer " +
                "SET firstName = ?, lastName = ?, country = ?, " +
                "postalCode = ?, phone = ?, email = ? " +
                "WHERE customerId = ?";
        int success = 0;
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, customer.firstName());
            statement.setString(2, customer.lastName());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postalCode());
            statement.setString(5, customer.phoneNumber());
            statement.setString(6, customer.email());
            statement.setInt(7, customerId);
            success = statement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
        if (success == 1)
            return getCustomerById(customerId);
        else
            return null;
    }

    /**
     * Helper method that handles the connection and results
     *
     * @param sql SQL query for getting customers from database
     * @return list of customers
     */
    private List<Customer> getCustomersFromDatabase(String sql) {
        List<Customer> customers = new ArrayList<>();
        // Open a connection (try with resources)
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            // Prepare statement
            //int id, String firstName, String lastName, String country, String postalCode, String phoneNumber, String email
            PreparedStatement statement = conn.prepareStatement(sql);
            // Handle result
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Customer customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
                customers.add(customer);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return customers;
    }
}
