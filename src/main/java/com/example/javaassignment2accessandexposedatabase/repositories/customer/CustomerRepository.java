package com.example.javaassignment2accessandexposedatabase.repositories.customer;

import com.example.javaassignment2accessandexposedatabase.models.Customer;
import com.example.javaassignment2accessandexposedatabase.models.CustomerCountry;
import com.example.javaassignment2accessandexposedatabase.models.CustomerGenre;
import com.example.javaassignment2accessandexposedatabase.models.CustomerSpender;

import java.util.List;

public interface CustomerRepository {
    List<Customer> getAllCustomers();

    Customer addCustomer(Customer customer);

    Customer getCustomerById(int customerId);

    Customer getCustomerByName(String customerName);

    List<Customer> getCustomersWithLimitAndOffset(int limit, int offset);

    List<CustomerCountry> getNumberOfCustomersInEachCountry();

    List<CustomerSpender> getHighestSpenders();

    List<CustomerGenre> getMostPopularGenre(int customerId);

    Customer updateCustomer(int id, Customer customer);

    // int delete();
}
