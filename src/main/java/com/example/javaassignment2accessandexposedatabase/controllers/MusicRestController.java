package com.example.javaassignment2accessandexposedatabase.controllers;

import com.example.javaassignment2accessandexposedatabase.models.*;
import com.example.javaassignment2accessandexposedatabase.repositories.music.MusicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/music")
public class MusicRestController {

    @Autowired
    MusicRepository musicRepository;

    @GetMapping("/song/{songName}")
    public List<Song> getSong(@PathVariable String songName) {
        return musicRepository.getSongsByName(songName);
    }
}
