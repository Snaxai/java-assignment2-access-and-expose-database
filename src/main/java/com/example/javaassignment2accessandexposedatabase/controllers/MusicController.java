package com.example.javaassignment2accessandexposedatabase.controllers;

import com.example.javaassignment2accessandexposedatabase.models.Song;
import com.example.javaassignment2accessandexposedatabase.repositories.customer.CustomerRepository;
import com.example.javaassignment2accessandexposedatabase.repositories.music.MusicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("music")
public class MusicController {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private MusicRepository musicRepository;

    @GetMapping
    public String index(Model model) {
        model.addAttribute("artists", musicRepository.getRandomArtists(5));
        model.addAttribute("genres", musicRepository.getRandomGenres(5));
        model.addAttribute("tracks", musicRepository.getRandomTracks(5));
        return "home-page";
    }

    @GetMapping("/result")
    public String resultPage() {
        return "home-page";
    }

    @GetMapping("/result/{songName}")
    public String searchSong(@PathVariable String songName, Model model) {
        model.addAttribute("songs", musicRepository.getSongsByName(songName));
        return "result-page";
    }

    @GetMapping("/result/rest/{songName}")
    public List<Song> searchSongRest(@PathVariable String songName) {
        return musicRepository.getSongsByName(songName);
    }
}
