package com.example.javaassignment2accessandexposedatabase.controllers;

import com.example.javaassignment2accessandexposedatabase.models.*;
import com.example.javaassignment2accessandexposedatabase.repositories.customer.CustomerRepository;
import com.example.javaassignment2accessandexposedatabase.repositories.music.MusicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/customers")
public class CustomerRestController {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private MusicRepository musicRepository;

    @GetMapping
    public List<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    @GetMapping("/highest-spenders")
    public List<CustomerSpender> getHighestSpenders() {
        return customerRepository.getHighestSpenders();
    }

    @GetMapping("/per-country")
    public List<CustomerCountry> getNumberOfCustomersInEachCountry() {
        return customerRepository.getNumberOfCustomersInEachCountry();
    }

    @GetMapping("/id/{id}")
    public Customer getCustomerById(@PathVariable int id) {
        return customerRepository.getCustomerById(id);
    }

    @GetMapping("/name/{customerName}")
    public Customer getCustomerByName(@PathVariable String customerName) {
        return customerRepository.getCustomerByName(customerName);
    }

    @GetMapping("/limit-customers")
    public List<Customer> getCustomerWithLimitAndOffset(@RequestParam int limit, @RequestParam int offset) {
        return customerRepository.getCustomersWithLimitAndOffset(limit, offset);
    }

    @GetMapping("{customerId}/favorite-genre")
    public List<CustomerGenre> getMostPopularGenre(@PathVariable int customerId) {
        return customerRepository.getMostPopularGenre(customerId);
    }

    @PostMapping
    public Customer addCustomer(@RequestBody Customer customer) {
        return customerRepository.addCustomer(customer);
    }

    @PutMapping("{customerId}")
    public Customer updateCustomer(@PathVariable int customerId, @RequestBody Customer customer) {
        return customerRepository.updateCustomer(customerId, customer);
    }

    @GetMapping("music/song/{songName}")
    public List<Song> getSong(@PathVariable String songName) {
        return musicRepository.getSongsByName(songName);
    }
}
