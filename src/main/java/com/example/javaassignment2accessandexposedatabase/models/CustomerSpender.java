package com.example.javaassignment2accessandexposedatabase.models;

public record CustomerSpender(Customer customer, double spent) {
}
