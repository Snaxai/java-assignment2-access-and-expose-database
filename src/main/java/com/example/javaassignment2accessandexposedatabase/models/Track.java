package com.example.javaassignment2accessandexposedatabase.models;

public record Track(int id, String name) {
}
