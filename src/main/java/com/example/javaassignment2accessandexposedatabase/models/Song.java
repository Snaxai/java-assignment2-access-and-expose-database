package com.example.javaassignment2accessandexposedatabase.models;

public record Song(int id, String name, String artist, String album, String genre) {
}
