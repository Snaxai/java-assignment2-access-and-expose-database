package com.example.javaassignment2accessandexposedatabase.models;

public record CustomerCountry(String country, int numberOfCustomers) {
}
