package com.example.javaassignment2accessandexposedatabase.models;

public record CustomerGenre(String genreName, int timesBought) {
}
