package com.example.javaassignment2accessandexposedatabase.models;

public record Genre(int id, String name) {
}
