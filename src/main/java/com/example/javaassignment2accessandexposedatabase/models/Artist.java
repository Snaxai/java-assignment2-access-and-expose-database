package com.example.javaassignment2accessandexposedatabase.models;

public record Artist(int id, String name) {
}
