package com.example.javaassignment2accessandexposedatabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaAssignment2AccessAndExposeDatabaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaAssignment2AccessAndExposeDatabaseApplication.class, args);
    }

}
